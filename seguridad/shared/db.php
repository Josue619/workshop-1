<?php

require_once __DIR__ . '/../Db/PgConnection.php';
require_once __DIR__ . '/../Models/Usuario.php';

use DB\PgConnection;

$con = new PgConnection('postgres', 'postgres', 'lugares', 5432, 'localhost');
$con->connect();

$usuario_model = new Models\Usuario($con);
