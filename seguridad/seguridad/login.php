<?php
$title = 'Login';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
$email    = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user = $usuario_model->login($email, $password);
    var_dump($user);
    if ($user) {
        $_SESSION['usuario_id'] = $user['id'];
        return header('Location: /home');
    } else {
        echo "<h3>Usuario o Contraseña inválidos!</h3>";
    }
}
?>

<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <form method="POST">
        <div class="form-group">
          <label>Email:</label>
          <input type="email" name="email" class="form-control" required value="<?=$email?>">
        </div>
        <div class="form-group">
          <label>Contraseña:</label>
          <input type="password" name="password" class="form-control" required>
        </div>
        <input type="submit" class="btn btn-primary" value="Login!">
      </form>
    </div>
  </div>
</div>
